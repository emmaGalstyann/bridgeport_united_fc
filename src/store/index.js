import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: null,
        news: [
            {
                "id": 1,
                "img": "src/assets/img/news1.png",
                "title": "Maguire points the way forward for United",
                "date": "12 SEP 18",
                "category": "News",

            },
            {
                "id": 2,
                "img": "src/assets/img/news2.png",
                "title": "Maguire points the way forward for United",
                "date": "10 SEP 2019",
                "category": "Video"
            },
            {
                "id": 3,
                "img": "src/assets/img/news3.png",
                "title": "Maguire points the way forward for United",
                "date": "10 SEP 2019",
                "category": "Video",
            }
        ],
        players: [
            {
                "id": 1,
                "img": "src/assets/img/michaelScott.png",
                "name": "Michael",
                "secondName": "Scott",
                "number": 11,
                "position": "Forward",
                "appearences": 125,
                "goals": 112,
                "assists": 35
            },
            {
                "id": 2,
                "img": "src/assets/img/robinJohnson.png",
                "name": "Michael",
                "secondName": "Scott",
                "number": 15,
                "position": "Forward",
                "appearences": 125,
                "goals": 112,
                "assists": 35
            },
            {
                "id": 3,
                "img": "src/assets/img/player.png",
                "name": "Michael",
                "secondName": "Scott",
                "number": 1,
                "position": "Forward",
                "appearences": 125,
                "goals": 112,
                "assists": 35
            },
            {
                "id": 4,
                "img": "src/assets/img/michaelScott.png",
                "name": "Michael",
                "secondName": "Scott",
                "number": 2,
                "position": "Forward",
                "appearences": 125,
                "goals": 112,
                "assists": 35
            },
            {
                "id": 5,
                "img": "src/assets/img/michaelScott.png",
                "name": "Michael",
                "secondName": "Scott",
                "number": 1,
                "position": "Forward",
                "appearences": 125,
                "goals": 112,
                "assists": 35
            },
            {
                "id": 6,
                "img": "src/assets/img/michaelScott.png",
                "name": "Michael",
                "secondName": "Scott",
                "number": 2,
                "position": "Forward",
                "appearences": 125,
                "goals": 112,
                "assists": 35
            }
        ],
        teams: [
            {
                //id: 1,
                //position: 1,
                team: "Newtown Pride FC",
                points: 9
            },
            {
                //id: 2,
                //position: 2,
                team: "Waterbury Pontes FC",
                points: 6
            },
            {
                //id: 3,
                //position: 3,
                team: "Polonia Falcon",
                points: 6
            },
            {
                //id: 4,
                //position: 4,
                team: "Sporting Hartford",
                points: 4
            },
            {
                //id: 5,
                //position: 5,
                team: "Sporting Hartford",
                points: 3
            },
            {
                //id: 6,
                //position: 6,
                team: "Sporting Hartford",
                points: 3
            },
            /*{
              id: 7,
              position: "...",
              team: "",
              points: "..."
            },*/
            {
                //id: 8,
                //position: 8,
                team: "FC BRIDGEPORT UNITED",
                points: 1,
                active: true
            },
            {
                //id: 9,
                //position: 9,
                team: "Silk City",
                points: 0
            }
        ]
    },
    actions: {
        setNews({ commit }, payload) {
            commit("setNews", payload);
        },

        setPlayers({ commit }, payload) {
            commit("setPlayers", payload);
        }
    },
    mutations: {
        setNews(state, payload) {
            state.news = payload;
        },
        setPlayers(state, payload) {
            state.players = payload;
        },
    },
    getters: {
        news(state) {
            return state.news;
        },
        teams(state) {
            return state.teams;
        },
        players(state) {
            return state.players;
        }
    }
})