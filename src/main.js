import Vue from 'vue'

import router from "./router/";
import store from './store/';

import axios from 'axios'
import VueAxios from 'vue-axios'

import App from './App.vue';

import "./assets/style.scss";

Vue.use(VueAxios, axios);

new Vue({
  el: '#app',
  router,
  axios,
  store,
  render: h => h(App)
})
